# README #

Effy is a high level IO Utils library based on Java Native APIs. In order to use the libary, you need to be on Java 7
or above. You can download the latest jar from https://sourceforge.net/projects/effy/. Or you can checkout this project
and build your own jar.

### What is this repository for? ###

* This is the source code for the Effy Jar hosted on source forge
* 1.1 Current version is 1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Ideally you want to use the high level APIs of Effy. For that go to https://sourceforge.net/projects/effy/, download the latest
effy jar, add into your Eclipse Project and start using it.
* Documentation -  For latest API documentation please refer to http://anilpank.bitbucket.org/. This is where I maintain documentation
of all the Effy APIs.
* If you want to get deeper in how the APIs are implemented, then checkout the project and look at the source code

### Contribution guidelines ###
* You can go ahead and suggest any APIs/methods that you find missing in current third party IO Util libraries.  
* Drop the suggestions at my email anil.iitk@gmail.com or at twitter at @anilpank
* Please go ahead and report bugs.


### Who do I talk to? ###

* Repo owner or admin Anil Verma. My email address is anil.iitk@gmail.com

### LICENSE ###

* Distributed under the MIT License. (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT )
